" ===== change keybindings ===== "
" === disable arrow keys === "
"noremap <Up> <Nop>
"noremap <Down> <Nop>
"noremap <Left> <Nop>
"noremap <Right> <Nop>
"
"inoremap <Up> <Nop>
"inoremap <Down> <Nop>
"inoremap <Left> <Nop>
"inoremap <Right> <Nop>
"
"vnoremap <Up> <Nop>
"vnoremap <Down> <Nop>
"vnoremap <Left> <Nop>
"vnoremap <Right> <Nop>

" === hjkl -> snrt === "
nnoremap s h
nnoremap n j
nnoremap r k
nnoremap t l

vnoremap s h
vnoremap n j
vnoremap r k
vnoremap t l

" === map snrt to now unused hjkl === "
" still available: j, k

" == normal mode == "
" replace letter
nnoremap l r
nnoremap L R

" move in search mode
nnoremap h n
nnoremap H N

" == visual mode == "
vnoremap l r
vnoremap L R

" == insert mode == "
" text completion
"inoremap <c-h> <c-n>

" ===== editing ===== "

" ===== intendation ===== "
set sts=4 " softtabsstop (manual tabs)
set sw=4 " shiftwidth (automatic tabs)
set expandtab " use spaces for tabs
set hidden " having multiple files open in the backround
set smartindent
set autoindent

" ===== folding ===== "
set foldenable
set foldmethod=indent 

" ===== searching ===== "
set incsearch
set hlsearch
set ignorecase
set smartcase
noremap <silent> <Space> :silent noh<Bar>echo<CR>

" ===== highlight whitespace ===== "
highlight RedundantSpaces ctermbg=red guibg=red 
match RedundantSpaces /\s\+$/

" ===== windows, tabs, buffers ===== "
" tab: cycle through windows
nnoremap <silent> <tab> :silent wincmd w<cr>


" ===== colorscheme ===== "
syntax enable
"colorscheme ron
"colorscheme badwolf
"colorscheme everforest
"colorscheme elflord
"colorscheme koehler
"colorscheme slate 

" ===== plug-ins ===== "
runtime plugins.vim

" ===== status line ===== "
set laststatus=2
set timeoutlen=1000 ttimeoutlen=0
" set noshowmode

" ===== other ===== "
" For local replace
"nnoremap gr gd[{V%::s/<C-R>///gc<left><left><left>
"set ruler " show line and column number
set number " show line numbers left
